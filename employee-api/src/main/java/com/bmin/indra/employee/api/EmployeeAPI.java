package com.bmin.indra.employee.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bmin.indra.employee.business.EmployeeService;
import com.bmin.indra.employee.dto.EmployeeDTO;

@RestController
@RequestMapping("/api/employee")
public class EmployeeAPI {
	
	@Autowired
	private EmployeeService service;
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<EmployeeDTO> getById(
			@RequestParam(name = "id", required = true) Long id){
		try {
			EmployeeDTO dto = this.service.getByID(id);
			
			if(dto != null) {
				return new ResponseEntity<EmployeeDTO>(dto, HttpStatus.OK);
			}else {
				return new ResponseEntity<EmployeeDTO>(HttpStatus.NO_CONTENT);
			}
		}catch (Exception e) {
			e.printStackTrace();
			
			return new ResponseEntity<EmployeeDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

}
