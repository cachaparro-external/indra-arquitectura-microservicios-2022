package com.bmin.indra.employee.business;

import org.springframework.stereotype.Service;

import com.bmin.indra.employee.dto.EmployeeDTO;

@Service
public interface EmployeeService {
	
	public EmployeeDTO getByID(Long id);

}
