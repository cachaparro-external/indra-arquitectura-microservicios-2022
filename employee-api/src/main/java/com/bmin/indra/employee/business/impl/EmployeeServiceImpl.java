package com.bmin.indra.employee.business.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bmin.indra.employee.business.EmployeeService;
import com.bmin.indra.employee.dto.EmployeeDTO;
import com.bmin.indra.employee.persistent.entity.EmployeeEntity;
import com.bmin.indra.employee.persistent.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public EmployeeDTO getByID(Long id) {
		EmployeeDTO dto = new EmployeeDTO();
		
		Optional<EmployeeEntity> entityOpt = this.repository.findById(id);
		
		if(entityOpt.isPresent()) {
			EmployeeEntity entity = entityOpt.get();
			
			dto.setEmail(entity.getEmail());
			dto.setFullName(entity.getFirstName().concat(" ").concat(entity.getLastName()));
			dto.setHireDate(entity.getHireDate());
			dto.setId(entity.getId());
			dto.setJobId(entity.getJobId());
			dto.setSalary(entity.getSalary().longValue());
			
			return dto;
		}else {
			return null;
		}
	}

}
