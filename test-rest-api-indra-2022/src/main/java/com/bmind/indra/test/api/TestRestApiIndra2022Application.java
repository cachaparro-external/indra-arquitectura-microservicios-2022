package com.bmind.indra.test.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestRestApiIndra2022Application {

	public static void main(String[] args) {
		SpringApplication.run(TestRestApiIndra2022Application.class, args);
	}

}
