package com.bmind.indra.test.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bmind.indra.test.api.dto.EmployeeDTO;

@RestController
@RequestMapping("/api/employee")
public class EmployeeAPI {
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			value = "/id/{id}")
	public ResponseEntity<EmployeeDTO> getEmployeeByID(
			@PathVariable(name = "id") Long id,
			@RequestHeader(name = HttpHeaders.ACCEPT) String acceptHeader
			) {
		
		System.out.println("Accept: " + acceptHeader);
		
		if(id <= 50) {
			EmployeeDTO dto = new EmployeeDTO();
			dto.setBirthDate(LocalDate.now());
			dto.setFirstName("Jose");
			dto.setLastName("Rodriguez");
			dto.setIdNumber(id);
			
			return new ResponseEntity<EmployeeDTO>(dto, HttpStatus.OK);
		}else {
			return new ResponseEntity<EmployeeDTO>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/firstName/{fn}/lastName/{ln}")
	public ResponseEntity<List<EmployeeDTO>> getEmployeeByNames(
			@PathVariable(name = "fn") String firstName,
			@PathVariable(name = "ln") String lastName
			) {
		
		List<EmployeeDTO> list = this.getEmployees(firstName, lastName);
		
		return new ResponseEntity<List<EmployeeDTO>>(list, HttpStatus.OK);
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/names/{fn}/{ln}")
	public ResponseEntity<List<EmployeeDTO>> getEmployeeByNames2(
			@PathVariable(name = "fn") String firstName,
			@PathVariable(name = "ln") String lastName
			) {
		
		List<EmployeeDTO> list = this.getEmployees(firstName, lastName);
		
		return new ResponseEntity<List<EmployeeDTO>>(list, HttpStatus.OK);
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/full-name")
	public ResponseEntity<List<EmployeeDTO>> getEmployeeByNames3(
			@RequestParam(name = "firstName", required = true) String firstName,
			@RequestParam(name = "lastName", required = true) String lastName
			) {
		
		List<EmployeeDTO> list = this.getEmployees(firstName, lastName);
		
		return new ResponseEntity<List<EmployeeDTO>>(list, HttpStatus.OK);
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/full-name/{firstName}")
	public ResponseEntity<List<EmployeeDTO>> getEmployeeByNames4(
			@PathVariable(name = "firstName") String firstName,
			@RequestParam(name = "lastName", required = true) String lastName
			) {
		
		List<EmployeeDTO> list = this.getEmployees(firstName, lastName);
		
		return new ResponseEntity<List<EmployeeDTO>>(list, HttpStatus.OK);
	}
	
	@PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			value = "/filter")
	public ResponseEntity<List<EmployeeDTO>> getEmployeeByNames5(
			@RequestBody EmployeeDTO dto
			) {
		
		List<EmployeeDTO> list = this.getEmployees(dto.getFirstName(), dto.getLastName());
		
		return new ResponseEntity<List<EmployeeDTO>>(list, HttpStatus.OK);
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			     produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<EmployeeDTO> create(
			@RequestBody EmployeeDTO dto) {
		
		dto.setIdNumber(new Random(System.currentTimeMillis()).nextLong());
		
		return new ResponseEntity<EmployeeDTO>(dto, HttpStatus.OK);
	}
	
	@PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Void> update(
			@RequestBody EmployeeDTO dto) {
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			value = "/id/{id}")
	public ResponseEntity<Void> update2(
			@PathVariable(name = "id") Long id,
			@RequestBody EmployeeDTO dto) {
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/id/{id}")
	public ResponseEntity<Void> delete(
			@PathVariable(name = "id") Long id) {
		if(id < 50) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		}else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<EmployeeDTO> delete2(
			@PathVariable(name = "id") Long id) {
		EmployeeDTO dto = new EmployeeDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName("Carlos");
		dto.setLastName("Chaparro");
		dto.setIdNumber(id);
		
		return new ResponseEntity<EmployeeDTO>(dto, HttpStatus.OK);
	}
	
	/*@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			     produces = {MediaType.APPLICATION_JSON_VALUE},
			     value = "/merge")
	@PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
    			produces = {MediaType.APPLICATION_JSON_VALUE},
    			value = "/merge")*/
	@RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT},
			value = "/merge",
			consumes = {MediaType.APPLICATION_JSON_VALUE},
		     produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Void> merge(
			@RequestBody EmployeeDTO dto) {
		
		if(dto.getIdNumber() < 50) {
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}else {
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
				value = "/execute")
	public ResponseEntity<Void> ejecutarProcesoNomina(
			@RequestHeader(name = "username", required = true) String username,
			@RequestBody EmployeeDTO dto
			) {
		System.out.println("Ejecutando el proceso con el usuario: " + username);
		
		MultiValueMap<String, String> header = new HttpHeaders();
		
		header.add("idProceso", "2545");
		header.add("username", username);
		header.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE);
		
		return new ResponseEntity<Void>(header, HttpStatus.OK);
	}
	
	private List<EmployeeDTO> getEmployees(String firstName, String lastName){
		List<EmployeeDTO> list = new ArrayList<>();
		
		EmployeeDTO dto = new EmployeeDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		dto.setIdNumber(25L);
		
		list.add(dto);
		
		dto = new EmployeeDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		dto.setIdNumber(45L);
		
		list.add(dto);
		
		return list;
	}

}
